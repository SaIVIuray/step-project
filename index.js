document.addEventListener("DOMContentLoaded", function () {
    const tabsTitles = document.querySelectorAll(".tabs-title");
    const tabs = document.querySelectorAll(".tab");

    tabsTitles.forEach((title) => {
        title.addEventListener("click", function () {
            const tabId = title.getAttribute("data-tab");

            tabsTitles.forEach((t) => {
                t.classList.remove("active");
            });

            title.classList.add("active");

            tabs.forEach((tab) => {
                tab.style.display = "none";
            });

            const selectedTab = document.getElementById(tabId);
            if (selectedTab) {
                selectedTab.style.display = "flex";
            }
        });
    });
});

document.addEventListener("DOMContentLoaded", function () {
    const tabsTitles = document.querySelectorAll(".tabs-title");

    tabsTitles.forEach((title) => {
        title.addEventListener("click", function () {
            const cursor = title.querySelector(".cursor"); 

            tabsTitles.forEach((t) => {
                const tCursor = t.querySelector(".cursor");
                if (t !== title && tCursor) {
                    tCursor.style.visibility = "hidden";
                }
            });

            cursor.style.visibility = "hidden" ? "visible" : "hidden";
        });
    });
    const webDesignTab = document.querySelector(".tabs-title[data-tab='web-design']");
    const webDesignCursor = webDesignTab.querySelector(".cursor");
    webDesignCursor.style.visibility = "visible";
});

document.addEventListener("DOMContentLoaded", function () {
    const tabTitles = document.querySelectorAll(".Our-title");
    
    tabTitles.forEach(tabTitle => {
        tabTitle.addEventListener("click", () => {
            const category = tabTitle.getAttribute("data-category");

            const images = document.querySelectorAll(".Our-Work-Images .hover-trigger");
            
            images.forEach(image => {
                if (category === "All" || image.classList.contains(category)) {
                    image.style.display = "block";
                } else {
                    image.style.display = "none";
                }
            });
        });
    });
});

document.addEventListener("DOMContentLoaded", function () {
    const loadMoreButton = document.getElementById("load-more-button");
    const newImagesInfo = [
        { src: "./graphic design/graphic-design5.jpg", alt: "Graphic Design", class: "GD" },
        { src: "./graphic design/graphic-design6.jpg", alt: "Web Design", class: "WD"},
        { src: "./graphic design/graphic-design7.jpg", alt: "Landing Pages", class: "LP" },
        { src: "./graphic design/graphic-design8.jpg", alt: "Wordpress", class: "WP"},
        { src: "./web design/web-design5.jpg", alt: "Graphic Design" , class: "GD"},
        { src: "./web design/web-design6.jpg", alt: "Web Design", class: "WD"},
        { src: "./web design/web-design7.jpg", alt: "Graphic Design" , class: "GD"},
        { src: "./graphic design/graphic-design9.jpg", alt: "Landing Pages", class: "LP" },
        { src: "./landing page/landing-page5.jpg", alt: "Wordpress", class: "WP"},
        { src: "./landing page/landing-page6.jpg", alt: "Landing Pages", class: "LP" },
        { src: "./landing page/landing-page7.jpg", alt: "Wordpress", class: "WP"},
        { src: "./wordpress/wordpress5.jpg", alt: "Web Design", class: "WD"},
    ];

    loadMoreButton.addEventListener("click", function () {
        const newImagesContainer = document.querySelector(".Our-Work-Images");
        newImagesInfo.forEach(info => {
            const wrapper = document.createElement('div');
            const img = document.createElement("img");
            const hoverImg = document.createElement("div");

            wrapper.classList.add('hover-trigger'); 
            wrapper.classList.add(info.class);

            img.src = info.src;
            img.alt = info.alt;

            hoverImg.classList.add("hover-img"); 

            const hoverImgImg = document.createElement("img");
            hoverImgImg.src = "./Sections background/icon.png";
            hoverImgImg.alt = "";

            const h1 = document.createElement("h1");
            h1.textContent = "creative design";

            const p = document.createElement("p");
            p.textContent = info.alt;

            hoverImg.appendChild(hoverImgImg);
            hoverImg.appendChild(h1);
            hoverImg.appendChild(p);

            wrapper.appendChild(img);
            wrapper.appendChild(hoverImg); 

            newImagesContainer.appendChild(wrapper);   

        });
        loadMoreButton.style.display = "none";

        const hoverImages = document.querySelectorAll(".hover-trigger");

        hoverImages.forEach(image => {
            image.addEventListener("mouseenter", function () {
                const hoverImg = image.querySelector(".hover-img");
                const img = image.querySelector("img");
                if (hoverImg && img) {
                    hoverImg.style.display = "flex";
                    img.style.display = "none";
                }
            });

            image.addEventListener("mouseleave", function () {
                const hoverImg = image.querySelector(".hover-img");
                const img = image.querySelector("img");
                if (hoverImg && img) {
                    hoverImg.style.display = "none";
                    img.style.display = "flex";                   
                }
            });
        });
    });
});



$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
    
});

$('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    centerMode: true,
    focusOnSelect: true,
    arrows: true
});

$('.slider-nav .slick-slide.slick-current').first().css('transform', 'translateY(-15px)');
$('.slider-for').on('afterChange', function (event, slick, currentSlide) {

  $('.slider-nav .slick-slide').css('transform', 'none');
  $('.slider-nav .slick-slide.slick-current').css('transform', 'translateY(-15px)');
  $('.slider-nav .slick-slide.slick-current').css('transition', 'transform 0.3s ease'); 

});
  
$(document).ready(function(){
    $('.WP-text > div').not('[data-slide-index="0"]').hide();

    $('.slider-for').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var imgIndex = $(slick.$slides[nextSlide]).find('img').data('slide-index');
        
        $('.WP-text > div').hide();

        $('[data-slide-index="' + imgIndex + '"]').show();
    });
});





